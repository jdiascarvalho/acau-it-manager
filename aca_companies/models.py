# - coding: utf-8 -

#Acaua IT Manager is free software: you can redistribute it and/or modify
#it under the terms of the GNU Lesser General Public License as published by
#the Free Software Foundation, either version 3 of the License, or
#(at your option) any later version.

#Acaua IT Manager is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser 
#General Public License for more details.

#You should have received a copy of the GNU Lesser General Public License along 
#with this program. If not, see <http://www.gnu.org/licenses/>.

from django.db import models
from django.utils.translation import ugettext as _

class Company(models.Model):
    u'''
        Company's model class, this class controls companies entities 
    '''
    
    name = models.CharField(max_length=40,
                            db_index=True,
                            blank=False,
                            db_column='name',
                            help_text=_(u"The company's name"))
    
    address = models.CharField(max_length=70,
                               db_column='address',
                               help_text=_(u"The company's adress"))
    
    email_address = models.EmailField(db_column='email',
                                      help_text=
                                      _(u"The company's email address"))
    
    address_number = models.CharField(max_length=40,
                                     db_index=False,
                                     db_column='addressno',
                                     help_text=_(u'The company address number'))
    
    
    
    
    
    class Meta:
        db_table='companies'