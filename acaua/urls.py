# - coding: utf-8 -

#Acaua IT Manager is free software: you can redistribute it and/or modify
#it under the terms of the GNU Lesser General Public License as published by
#the Free Software Foundation, either version 3 of the License, or
#(at your option) any later version.

#Acaua IT Manager is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser 
#General Public License for more details.

#You should have received a copy of the GNU Lesser General Public License along 
#with this program. If not, see <http://www.gnu.org/licenses/>.

from django.conf.urls import patterns
from django.conf.urls import url

urlpatterns = patterns('',
    
    ###########################################################################
    #
    #    Dashboard 
    #
    ###########################################################################
    url(r'^$', 'aca_dashboard.views.dashboard_index', name='aca_dashboard'),
    url(r'^management/$','aca_dashboard.views.management_index',
        name='aca_management'),
    ###########################################################################
    #
    # Companies
    #
    ###########################################################################
    url(r'^management/companies/$','aca_companies.views.companies_index',
        name='aca_companies'),
                        
    ###########################################################################
    #
    # Reporting
    #
    ###########################################################################
    url(r'^reporting$','aca_reporting.views.reporting_index', 
        name='aca_reporting'),

    ###########################################################################
    #
    # Application's Login
    #
    ###########################################################################
    url(r'^login/$','django.contrib.auth.views.login',
        {'template_name':'aca_login.html'}),
    url(r'^logout/$','django.contrib.auth.views.logout_then_login',
        {'login_url':'/login/'}),
    
    # Uncomment the admin/doc line below to enable admin documentation:
    # url(r'^admin/doc/', include('django.contrib.admindocs.urls')),

    # Uncomment the next line to enable the admin:
    # url(r'^admin/', include(admin.site.urls)),
)
