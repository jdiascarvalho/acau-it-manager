#!/usr/bin/env python

#Acaua IT Manager is free software: you can redistribute it and/or modify
#it under the terms of the GNU Lesser General Public License as published by
#the Free Software Foundation, either version 3 of the License, or
#(at your option) any later version.

#Acaua IT Manager is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser 
#General Public License for more details.

#You should have received a copy of the GNU Lesser General Public License along 
#with this program. If not, see <http://www.gnu.org/licenses/>.

import os
import sys

if __name__ == "__main__":
    os.environ.setdefault("DJANGO_SETTINGS_MODULE", "acaua.settings")

    from django.core.management import execute_from_command_line

    execute_from_command_line(sys.argv)
