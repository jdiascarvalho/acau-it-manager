/*
    Acaua IT Manager is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.
    
    Acaua IT Manager is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser 
    General Public License for more details.
    
    You should have received a copy of the GNU Lesser General Public License along 
    with this program. If not, see <http://www.gnu.org/licenses/>.
*/

jQuery.acauaLoader = function(url, options) {
  options = $.extend(options || {}, {
    dataType: "script",
    cache: true,
    url: url
  });
  return jQuery.ajax(options);
};

/* alert-info - alert-success - alert-error */
jQuery.acauaAlertMessage = function(cssClass, message, seconds){
   var msgId = 'msgId'+new Date().getTime();
   var dom = '<div id="'+msgId+'" style="margin-left:10px; position:fixed; width:95%; z-index:9999;" class="alert alert-block '+cssClass+' fade in"> '+
              '    <a class="close" data-dismiss="alert" href="#">&times;</a> '+
              '    <ul> '+
              '         <li>'+message+'</li> '+
              '    </ul> '+
              '</div> ';
   $('.navbar').after(dom);
   $('#'+msgId).alert();
   //Default 3000
   seconds = seconds == undefined ? 3000 : seconds;
   var timer = $.timer(function(){
       $('#'+msgId).alert('close');           
       this.stop();
   });
   timer.set({ time : seconds, autostart: true});
    $('#'+msgId).bind('closed', function () {
       $('#'+msgId).remove();
    })
};



jQuery.acauaAlertYesNo = function(caption,message,yesCallback,noCallback)
{
    var selector = undefined;
    
    var ynId = 'yn'+new Date().getTime();
    $('body').append(
        '<div id ="'+ynId+'" class="modal fade hide">'+
        '   <div class="modal-header"> '+
        '       <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button> '+
        '       <h3>'+caption+'</h3> '+
        '   </div> '+
        '   <div class="modal-body"> '+message+'</div>'+
        '   <div class="modal-footer"> '+
        '       <a id="btYes" href="javascript:void(0)" class="btn"><i class="icon-fan-accept"></i>Sim</a>'+
        '       <a id="btNo" href="javascript:void(0)" class="btn"><i class="icon-fan-cancel"></i>Não</a>'+
        '   </div> '+
        '</div>');    
    //Show YesNo Alert    
    $('#'+ynId).modal({backdrop:true});
    
    //Yes Callback
    $('#'+ynId+' #btYes').click(function(){
        $('#'+ynId).modal('hide');
        selector = 'yes';
    });
    
    //No Callback
    $('#'+ynId+' #btNo').click(function(){
        $('#'+ynId).modal('hide');
        selector = 'no';
    });

    //Remove Message DOM Element
    $('#'+ynId).on('hidden', function () {
        $('#'+ynId).remove();
        if (selector == 'yes'){
            if (yesCallback != undefined){
                yesCallback();        
            }
        }
        if (selector == 'no'){
            if (noCallback != undefined){
                noCallback();        
            }            
        }
    });
    
};

jQuery.acauaAlertOK = function (caption,message,okCallback){
    var alertId = 'bjalert'+new Date().getTime();
    $('body').append(
        '<div id ="'+alertId+'" class="modal fade hide">'+
        '   <div class="modal-header"> '+
        '       <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button> '+
        '       <h3>'+caption+'</h3> '+
        '   </div> '+
        '   <div class="modal-body"> '+message+'</div>'+
        '   <div class="modal-footer"> '+
        '       <a id="btok" href="javascript:void(0)" class="btn"><i class="icon-fan-accept"></i>Ok</a>'+
        '   </div> '+
        '</div>');    
    //Show Alert    
    $('#'+alertId).modal('show');
    
    //Ok Callback
    $('#'+alertId+' #btok').click(function(){
        $('#'+alertId).modal('hide');
        if (okCallback != undefined){
            okCallback();        
        }
    });
    
    //Remove Message DOM Element
    $('#'+alertId).on('hidden', function () {
        $('#'+alertId).remove();
    });
};
