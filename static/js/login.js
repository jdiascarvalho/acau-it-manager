/*
    Acaua IT Manager is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.
    
    Acaua IT Manager is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser 
    General Public License for more details.
    
    You should have received a copy of the GNU Lesser General Public License along 
    with this program. If not, see <http://www.gnu.org/licenses/>.
*/

function LoginController(){

}

LoginController.prototype = {
    /**
     * This method starts Login Window behavior
     * @param {string} domId
     * ModalWindow DOM Id
     * @param {string} focusId
     * Dom Element Id that receives focus after Window Start
     * @param {boolean} backdrop
     * Do Application background turned to gray such as a Modal window? (default:false)
     * @param {boolean} keyboard 
     * Do Esc key closes window? (default:false)
     */
    startLoginWindow : function(domId, focusId, backdrop, keyboard) {
      if (domId == undefined){
          throw new Error('domId property must be defined!');
      }
      if (focusId == undefined){          
          throw new Error('focusId property must be defined!');
      }
      backdrop = typeof(backdrop) === 'undefined' ? false : backdrop;
      keyboard = typeof(keyboard) === 'undefined' ? false : keyboard;            
      $(domId).modal({
          backdrop:backdrop,
          keyboard:keyboard,
          show:true
      });
      //Set focus to focus object
      $(focusId).focus();
      //Activate error alert
      $(".alert").alert();
    }
}


var login = new LoginController();

$(document).ready(function(){
    login.startLoginWindow('#loginWindow','#id_username');     
});
