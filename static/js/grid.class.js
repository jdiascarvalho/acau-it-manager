function acaGrid(){
    
}

acaGrid.prototype = 
{        
        queryColumns : function(url, colCallback){
            $.ajax({
                type: 'GET',
                url:url,
                datatype:'json',
            }).done(function(data){
                if (colCallback != undefined){
                    colCallback(data);      
                }
            });
        },
       initialize:function(
           tableId, tableHeigth, dataUrl, columns, sortName, sortOrder,
           successCallback){
          var options = 
          {
            url: dataUrl,
            dataType: 'json',
            colModel: columns,
            sortname: sortName,
            sortorder: sortOrder,
            usepager: true,
            useRp: false,
            rp:7,
            height: tableHeigth,
            showTableToggleBtn: false,
            resizable: false,
            singleSelect: true,
            errormsg: 'Erro ao conectar os dados',
            pagestat: 'Mostrando {from} de {to}',
            pagetext: 'Página',
            outof: 'de',
            findtext: 'Buscar',
            procmsg: 'Processando... por favor aguarde...',
            nomsg: 'Não há itens para mostrar',
            onSuccess: function(){
                if ((tableId + ' #row1').length>0){
                    $(tableId +' tr:first').addClass('trSelected');
                }
                if (successCallback != undefined){
                    successCallback();
                }
              }
          }
          $(tableId).flexigrid(options);          
       }
}