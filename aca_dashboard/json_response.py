# - coding: utf-8 -

#Acaua IT Manager is free software: you can redistribute it and/or modify
#it under the terms of the GNU Lesser General Public License as published by
#the Free Software Foundation, either version 3 of the License, or
#(at your option) any later version.

#Acaua IT Manager is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser 
#General Public License for more details.

#You should have received a copy of the GNU Lesser General Public License along 
#with this program. If not, see <http://www.gnu.org/licenses/>.

from django.http import HttpResponse
from django.utils import simplejson

class JsonResponse(HttpResponse):
    def __init__(self,data):
        HttpResponse.__init__(self,
            content=simplejson.dumps(data),
            mimetype='application/json'
    )