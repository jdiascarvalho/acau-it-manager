# - coding: utf-8 -

#Acaua IT Manager is free software: you can redistribute it and/or modify
#it under the terms of the GNU Lesser General Public License as published by
#the Free Software Foundation, either version 3 of the License, or
#(at your option) any later version.

#Acaua IT Manager is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser 
#General Public License for more details.

#You should have received a copy of the GNU Lesser General Public License along 
#with this program. If not, see <http://www.gnu.org/licenses/>.

from django.core.paginator import Paginator
from aca_dashboard.json_response import JsonResponse

class GridResponse(object):
    
    ACTION_BUTTONS = "<a href='{0}' class='btn btedit' \
    style='width:10px;heigth:10px;'>\
        <i class='icon-fan-pencil'></i>\
    </a>\
    <a href='javascript:void(0)' data-role='{1}' class='btn btdel' style='width:10px;heigth:10px;'>\
        <i class='icon-fan-delete'></i>\
    </a>"
    
    def __init__(self, request):        
        self.sortname = request.REQUEST.get('sortname','id')
        self.page = request.REQUEST.get('page',1)
        self.sortorder = request.REQUEST.get('sortorder','asc')
        self.rp = request.REQUEST.get('rp',9)
        self.qtype = request.REQUEST.get('qtype','')
        self.query = request.REQUEST.get('query','')
    
        #Combine sortename for sort order querys
        self.sortname = self.sortname if self.sortorder == "asc" \
            else str('-'+self.sortname)
        
        object.__init__(self)

    def get_grid_response(self,rows,usePagination=True):
        '''
         Returns JSON suitable for feeding Flexigrid.
         @type rows: [{'id': 1, 'cell': ['field1, 'field2', ...]}, ...]
         @param use_paginator: Set this to false if you are going to do 
            pagination outside this class, for instance if you have a very
            large set of objects and do not want to fetch all rows.   
        '''
                 
        p = Paginator(rows, self.rp)

        if (usePagination):
            rows = p.page(self.page).object_list
        json_dict = {
            'page':self.page,
            'total':p.count,
            'rows': rows
        }
        return JsonResponse(json_dict)